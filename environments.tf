module "development" {
  source                   = "./environment"
  gitlab_environment_scope = "development"
  kubeconfig               = data.scaleway_k8s_cluster.dev.kubeconfig[0]
  base_domain              = var.dev_base_domain
  namespace                = "${var.old_project_slug}-development"
  gitlab_project_ids       = local.gitlab_project_ids
  old_project_slug         = var.old_project_slug
  project_slug             = var.project_slug
  project_name             = var.project_name

  namespace_quota_max_cpu    = 9
  namespace_quota_max_memory = "18Gi"

  monitoring_org_id = random_string.development_secret_org_id.result

  smtp_host     = var.development_collectivite_fr_tem_host
  smtp_password = var.development_collectivite_fr_tem_secret_key
  smtp_port     = var.development_collectivite_fr_tem_port
  smtp_user     = var.development_collectivite_fr_tem_user

  scaleway_project_config = var.scaleway_project_config
  providers = {
    scaleway.legacy = scaleway.scaleway_development
    scaleway        = scaleway.scaleway_project
  }
}

module "production" {
  source                   = "./environment"
  gitlab_environment_scope = "production"
  kubeconfig               = data.scaleway_k8s_cluster.prod.kubeconfig[0]
  base_domain              = var.prod_base_domain
  namespace                = var.old_project_slug
  gitlab_project_ids       = local.gitlab_project_ids
  old_project_slug         = var.old_project_slug
  project_slug             = var.project_slug
  project_name             = var.project_name

  namespace_quota_max_cpu    = 9
  namespace_quota_max_memory = "22Gi"

  monitoring_org_id = random_string.production_secret_org_id.result

  smtp_host     = var.production_collectivite_fr_tem_host
  smtp_password = var.production_collectivite_fr_tem_secret_key
  smtp_port     = var.production_collectivite_fr_tem_port
  smtp_user     = var.production_collectivite_fr_tem_user

  scaleway_project_config = var.scaleway_project_config
  providers = {
    scaleway.legacy = scaleway.scaleway_production
    scaleway        = scaleway.scaleway_project
  }
}
