module "namespace" {
  source            = "gitlab.com/vigigloo/tf-modules/k8slimitednamespace"
  version           = "2.0.2"
  namespace         = var.project_slug
  max_cpu_requests  = "11"
  max_memory_limits = "15Gi"
  project_name      = "Metabase"
  project_slug      = "metabase"

  default_container_cpu_requests  = "10m"
  default_container_memory_limits = "16Mi"
}

resource "scaleway_object_bucket" "db_backups" {
  name = "${var.project_slug}-db-backups"
}
module "postgresql" {
  source  = "gitlab.com/vigigloo/tools-k8s-crunchydata/pgcluster"
  version = "0.0.22"

  chart_name = "postgresql"
  namespace  = module.namespace.namespace

  pg_replicas = 0

  pg_volume_size = "5Gi"

  pg_backups_volume_enabled             = true
  pg_backups_volume_size                = "30Gi"
  pg_backups_volume_full_schedule       = "15 4 * * 0"
  pg_backups_volume_incr_schedule       = "15 4 * * 1-6"
  pg_backups_volume_full_retention      = 4
  pg_backups_volume_full_retention_type = "count"

  pg_backups_s3_enabled       = true
  pg_backups_s3_bucket        = scaleway_object_bucket.db_backups.name
  pg_backups_s3_region        = "fr-par"
  pg_backups_s3_endpoint      = "s3.fr-par.scw.cloud"
  pg_backups_s3_access_key    = var.scaleway_project_config.access_key
  pg_backups_s3_secret_key    = var.scaleway_project_config.secret_key
  pg_backups_s3_full_schedule = "45 4 * * 0"
  pg_backups_s3_incr_schedule = "45 4 * * 1-6"
  values = [
    file("${path.module}/db_resources.yaml"),
    <<-EOT
      imagePgBackRest: null
      imagePostgres: null
      postgresVersion: 14
    EOT
  ]
}
