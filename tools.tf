module "metabase" {
  source       = "./tools/metabase"
  project_slug = "${var.project_slug}-metabase"
  hostname     = "metabase.${var.prod_base_domain}"

  scaleway_project_config = var.scaleway_project_config

  monitoring_org_id = random_string.production_secret_org_id.result

  smtp_host         = var.metabase_smtp_host
  smtp_port         = var.metabase_smtp_port
  smtp_user         = var.metabase_smtp_user
  smtp_password     = var.metabase_smtp_password
  smtp_security     = "tls"
  smtp_from_address = var.metabase_smtp_user
  smtp_from_name    = "Metabase Dotations Locales"
  providers = {
    helm       = helm.production
    kubernetes = kubernetes.production
    scaleway   = scaleway.scaleway_project
  }
}
