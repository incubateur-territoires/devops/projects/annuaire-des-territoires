data "scaleway_k8s_cluster" "prod" {
  provider   = scaleway.scaleway_production
  cluster_id = var.scaleway_cluster_production_cluster_id
}

data "scaleway_k8s_cluster" "dev" {
  provider   = scaleway.scaleway_development
  cluster_id = var.scaleway_cluster_development_cluster_id
}

data "gitlab_project" "backend" {
  id = 40477931
}
data "gitlab_project" "frontend" {
  id = 40843015
}
data "gitlab_project" "cronjobs" {
  id = 54399050
}

locals {
  gitlab_project_ids = {
    backend  = data.gitlab_project.backend.id
    frontend = data.gitlab_project.frontend.id
    cronjobs = data.gitlab_project.cronjobs.id
  }
}
