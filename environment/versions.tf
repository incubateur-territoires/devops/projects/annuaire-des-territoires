terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
    }
    helm = {
      source = "hashicorp/helm"
    }
    kubernetes = {
      source = "hashicorp/kubernetes"
    }
    scaleway = {
      source  = "scaleway/scaleway"
      version = "~> 2.8.0"
      configuration_aliases = [
        scaleway.legacy,
        scaleway,
      ]
    }
  }
  required_version = ">= 0.14"
}
