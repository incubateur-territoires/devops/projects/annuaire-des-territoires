locals {
  db_backups      = contains(["development", "production"], var.gitlab_environment_scope)
  backup_endpoint = "s3.fr-par.scw.cloud"
  backup_region   = "fr-par"
}

resource "scaleway_object_bucket" "db_backups" {
  name = "${var.project_slug}-${local.environment_name}-db-backups"
}
