variable "base_domain" {
  type = string
}

variable "scaleway_project_config" {
  type = object({
    project_id = string
    access_key = string
    secret_key = string
  })
  sensitive = true
}

variable "old_project_slug" {
  type = string
}
variable "project_slug" {
  type = string
}

variable "project_name" {
  type = string
}

variable "gitlab_environment_scope" {
  type = string
}
locals {
  environment_name = var.gitlab_environment_scope == "*" ? "review" : var.gitlab_environment_scope
}

variable "namespace" {
  type = string
}

variable "gitlab_project_ids" {
  type = object({
    backend  = number
    frontend = number
    cronjobs = number
  })
}

variable "namespace_quota_max_cpu" {
  type    = number
  default = 2
}

variable "namespace_quota_max_memory" {
  type    = string
  default = "12Gi"
}

variable "monitoring_org_id" {
  type = string
}

variable "smtp_user" {
  type = string
}
variable "smtp_password" {
  type      = string
  sensitive = true
}
variable "smtp_host" {
  type = string
}
variable "smtp_port" {
  type = string
}
