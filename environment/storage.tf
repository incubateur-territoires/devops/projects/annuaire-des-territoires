locals {
  environment = replace(var.gitlab_environment_scope, "*", "review")
}

resource "scaleway_object_bucket" "backend_storage" {
  provider = scaleway
  name     = "${var.project_slug}-${local.environment}-storage"
}
